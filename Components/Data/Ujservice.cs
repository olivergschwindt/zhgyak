﻿namespace gyak.Components.Data
{

    public class Ujservice
    {
        public static string[] kepcimek = { "/kep1.jpg", "/kep2.jpg", "/kep3.jpg", "/kep4.jpg" };
        private static int szamlalo {get; set;}
        public static async Task<string[]> NovelAsync()
        {
            string[] seged = new string[3];
            szamlalo++;
            if (szamlalo < kepcimek.Length - 2)
            {
                seged=await GetKepekAsync();
            }
            return seged;


        }
        public static async Task<string[]> CsokkentAsync()
        {
            string[] seged = new string[3];
            szamlalo--;
            if (szamlalo >= 0)
            {
                seged = await GetKepekAsync();
            }
            return seged;
        }
        public static Task<string[]> GetKepekAsync()
        {
            szamlalo = 0;
            string[] seged=new string[3];
            seged[0] = kepcimek[0+szamlalo];
            seged[1] = kepcimek[1+szamlalo];
            seged[2] = kepcimek[2 + szamlalo];
            return Task.FromResult(seged);
        }
        public static Task<string> GetNevAsync()
        {
            return Task.FromResult("nem tudom");
        }
    }

}
